import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Person } from '../models/person';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  persons : Person[];
  readonly URL_API = 'http://localhost:3000/api/persons';
  selectPerson: Person;

  constructor(private http: HttpClient) { }

  getPersons(){
    return this.http.get(this.URL_API);
  }
  postPerson(person: Person){
    return this.http.post(this.URL_API, person);
  }

  putPerson(person: Person){
    return this.http.post(this.URL_API+ `${person.id_}`, person);
  }


  deletePerson(_id: string){
    return this.http.delete(this.URL_API+ `${_id}`);
  }
}
