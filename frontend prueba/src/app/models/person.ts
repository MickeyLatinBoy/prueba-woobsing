export class Person {

    id_ : string;
    name : string;
    last_name : string;
    email : string;
    phone : number;
    address : string;
}
