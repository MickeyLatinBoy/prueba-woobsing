const Person = require('../models/person');
const PersonController = {};


PersonController.getPersons = async (req, res) => {
const persons = await Person.find();
res.json(persons);
};

PersonController.createPerson = async (req, res) => {
    const person = new Person(req.body);
    await person.save();
    res.json({
        status: "OK",
        messagge: "Fue creado"
    });
};

PersonController.getPerson = async(req, res) => {
    const person = await Person.findById(req.params.id);
    res.json(person);
    console.log('se obtuvieron empleados');
};


PersonController.editPerson = async(req, res) => {
    const {id} = req.params;
    const person = {
        name: req.body.name,
        last_name: req.body.last_name,
        email: req.body.email,
        phone: req.body.phone,
        address: req.body.address
    }
    await Person.findByIdAndUpdate(id, {$set: person}, {new: true});
    res.json({status: "Persona Actualizada"});
};

PersonController.deletePerson = async (req, res) => {
    await Person.findByIdAndDelete(req.params.id);
    res.json({status: "Persona eliminada"});
};

module.exports = PersonController;
