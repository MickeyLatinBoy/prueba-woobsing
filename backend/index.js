const express = require('express');
const morgan = require('morgan');
const app = express();

const { mongoose } = require('./database');

//configuracion
app.set('port', process.env.PORT || 3000);

//middlewares
app.use(morgan('dev'));
//esto es para que sistema operativo pueda interactuar con objetivos js
app.use(express.json());

//rutas
app.use('/api/persons', require('./routes/person.routes'));

//inicio del server
app.listen(app.get('port'), () => {
    console.log('servidor corriendo en el puerto 3000');
});
